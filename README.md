# Xoio #

Just a series of Cinder related things that I've picked up and wanted to arrange into something a bit more re-useable. 

Some snippets and arrangements are of my own design, other portions are bits of code that I've found and/or reverse engineered in the course of trying to get better at C++ and OpenGL.

### How do I get set up? ###

This is running off of Cinder's "glNext" branch which currently hasn't been merged into a official release quite yet. You'll have to 
follow their directions for [getting the source on Github](http://libcinder.org/docs/welcome/GitSetup.html) and make sure to pull the 'glNext' branch in the process. 

After you have the source, you should place this source into a folder called apps in your documents folder(this is of course assuming you're on OSX which I am). You can of course change the CINDER_PATH variable in the project
settings to something else if you'd like to move the project elsewhere. 

Some of it works. Some of it probably does not. I don't feel that it's complete enough to integrate into a actual project just yet. 

Please feel free to use whatever looks useful.