#version 150

uniform sampler2D backbuffer;
uniform sampler2D tex0;
uniform vec2 size0;



in vec2 res;
in vec2 mouse;
in float currentTime;
in vec4		Color;
in vec3		Normal;
in vec2		TexCoord;

out vec4 oColor;

void main(){
    vec3 normal = normalize( -Normal );
    float diffuse = max( dot( normal, vec3( 0, 0, -1 ) ), 0 );

    vec4 one = texture( backbuffer, TexCoord);
    vec4 two = texture( tex0, TexCoord );
    oColor = two;
}