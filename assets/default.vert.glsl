#version 150

uniform vec2 mousePos;
uniform float time;
uniform vec2 resolution;

out vec2 res;
out vec2 mouse;
out float currentTime;

uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec4		ciPosition;
in vec2		ciTexCoord0;
in vec3		ciNormal;
in vec4		ciColor;

out highp vec2	TexCoord;
out lowp vec4	Color;
out highp vec3	Normal;
void main(){
    
    currentTime = time;
    mouse = mousePos;
    res = resolution;
    
    gl_Position	= ciModelViewProjection * ciPosition;
    Color 		= ciColor;
    TexCoord	= ciTexCoord0;
    Normal		= ciNormalMatrix * ciNormal;

}