//
//  DisplayObject.h
//  Xoio
//
//  Created by Joseph Chow on 12/20/14.
//
//

#ifndef __Xoio__DisplayObject__
#define __Xoio__DisplayObject__

#include "cinder/gl/Fbo.h"
#include "cinder/gl/gl.h"
#include "cinder/Vector.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Vao.h"
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Shader.h"
#include "cinder/Vector.h"
#include "cinder/gl/Batch.h"

#include <string>
#include <vector>

#define STRINGIFY(A) #A

namespace xoio {
       /**
        *  Alias for a Vao object.
        *  A way to group several bits of
        *  extra data along with a VaoRef
        */
        struct Attribute{
            std::string name;
            int index;
            int size;
        };
    
    class DisplayObject {
        
    protected:
        //shader used for rendering
        ci::gl::GlslProgRef renderShader;
        
        //holds the positions of all the vertices
        std::vector<ci::vec3> vertices;
        
        //buffer and vao
        ci::gl::VboRef buffer;
        ci::gl::VaoRef attributes;
        
        //attributes relating to buffer and vao
        //stores information about various attributes
        std::vector<Attribute>attributeData;
        
        //stores varyings that get passed between shaders
        std::vector<std::string>varyings;
        
        //keeps track of the number of attributes being used
        int attributeCounter;
        
        GLenum attributeType;
        GLboolean normalizeValues;
        GLsizei stride;
        GLvoid * offset;
        
        
    public:
        DisplayObject();
        
        /**
         *  Loads the rendering shader. If a path is supplied
         *  it will try to find both the vertex and fragment
         *  shaders along the same path
         *  ie <some path to file>_<vertex or frag>.glsl
         *
         *  If no path is supplied, a default rendering shader is used.
         */
        void loadRenderShader(std::string path="");
        
        /**
         *  Adds a vertex in creation of the object
         */
        void vertex(ci::vec3 position);
        
        /**========== ATTRIBUTES ==========*/
        
        /**
         *  Adds the name of a Varying to 
         *  keep track of.
         */
        void addVarying(std::string name);
        
        void addAttribute(std::string name,int size);
        
        void enableAttribute(std::string name);
        
        
    };

}
#endif /* defined(__Xoio__DisplayObject__) */
