//
//  HttpClient.h
//  Nosh
//
//  Created by Joseph Chow on 5/12/15.
//
//

#ifndef __Nosh__HttpClient__
#define __Nosh__HttpClient__

#include <stdio.h>
#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/signals2.hpp>


#ifdef CINDER_MAC
#include "cinder/app/App.h"
#include "cinder/Url.h"
#include "cinder/Utilities.h"
#endif


/**
 *  A simple wrapper around boost::asio.
 *  Adapted from Paul Houx's Cinder samples
 *  https://github.com/paulhoux/Cinder-Samples
 */
namespace xoio {
    namespace network {
        
        typedef std::shared_ptr<class HttpClient>HttpClientRef;
        class HttpClient{
            
            //! asio service for use
            boost::asio::io_service ioservice;
            
            //! socket for use
            boost::asio::ip::tcp::socket mSocket;
            
            //boost::asio::steady_timer	mHeartBeatTimer;
            boost::asio::steady_timer mReconnectTimer;
            
            boost::asio::ip::tcp::endpoint mEndPoint;
            
            //! buffer for reading in data
            boost::asio::streambuf mBuffer;
            
            //! character to expect from the server when terminating a line.
            std::string mDelimiter = "\0";
            
            //! indicates whether or not we're closing the connection
            bool isClosing = false;
            
            //! indicates whether or not we're connected
            bool isConnected = false;
            
        public:
            HttpClient();
            static HttpClientRef create(){
                return HttpClientRef(new HttpClient());
            }
            
            //! boost::signals to attach callback to.
            boost::signals2::signal<void(const boost::asio::ip::tcp::endpoint&)> sConnected;
            
            //! boost::signals to attach disconnected callback to.
            boost::signals2::signal<void(const boost::asio::ip::tcp::endpoint&)> sDisconnected;
            
            //! boost::signals to attach message callback to.
            boost::signals2::signal<void(const std::string&)> sMessage;
            
            //! initiate the connection
            void connect(boost::asio::ip::tcp::endpoint& endpoint);
            void disconnect();
            virtual void read();
            virtual void close();
            
            ///// CALLBACKS /////
            
            //! callback for connections
            virtual void handle_connect(const boost::system::error_code& error);
            
            //! callback for reading
            virtual void handle_read(const boost::system::error_code& error);
            
            //! callback for writing
            virtual void handle_write(const boost::system::error_code& error);
            
            ///// FUNCTIONS /////
            virtual void do_write(const std::string &msg);
            virtual void do_close();
            
            
            virtual void do_reconnect(const boost::system::error_code& error);
            virtual void do_heartbeat(const boost::system::error_code& error);
            
        };
    }
}

#endif /* defined(__Nosh__HttpClient__) */
