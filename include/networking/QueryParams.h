//
//  QueryParams.h
//  Nosh
//
//  Created by Joseph Chow on 5/13/15.
//
//

#ifndef __Nosh__QueryParams__
#define __Nosh__QueryParams__

#include <string>
#include <map>

namespace xoio{
    namespace network{
        class QueryParams {
            
            //!params that make up the query string
            std::map<std::string,std::string> params;
            
            //! the compiled query string
            std::string query = "?";
        public:
            QueryParams();
            
            //! add a parameter
            void addParam(std::string name, std::string param);
            
            //! get the value set for a particular key
            std::string getValue(std::string key);
            
            //! output compiled string
            std::string getString();
            
        };
    }
}

#endif /* defined(__Nosh__QueryParams__) */
