//
//  GetRequest.h
//  Nosh
//
//  Created by Joseph Chow on 5/12/15.
//
//

#ifndef __Nosh__GetRequest__
#define __Nosh__GetRequest__

#include "Request.h"
namespace xoio {
    namespace network{
        class GetRequest : public Request{
            
            //! io service to interact with things
            boost::asio::io_service &ioservice;
            
        public:
            GetRequest();
            GetRequest(std::string urlstring,boost::asio::io_service &service);
            
        };
    }
}

#endif /* defined(__Nosh__GetRequest__) */
