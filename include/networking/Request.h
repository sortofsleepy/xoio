//
//  Request.h
//  Nosh
//
//  Created by Joseph Chow on 5/12/15.
//
//

#ifndef __Nosh__Request__
#define __Nosh__Request__

#include <stdio.h>
#include <boost/asio.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <string>

namespace xoio {
    namespace network {
        class Request {
            
        protected:
            //! port number of applicable
            int port;
            
            //! endpoint for the request
            boost::asio::ip::tcp::endpoint	mEndPoint;
            
            //! builds the url into a boost endpoint
            void buildEndpoint(boost::asio::io_service service,std::string urlstring,int port=80);
        public:
            Request();
        };
    }
}


#endif /* defined(__Nosh__Request__) */
