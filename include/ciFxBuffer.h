//
//  ciFxBuffer.h
//  ciFx
//
//  Created by Joseph Chow on 1/14/15.
//
//

#ifndef __ciFx__ciFxBuffer__
#define __ciFx__ciFxBuffer__

#include "cinder/Vector.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Surface.h"
#include "cinder/Rand.h"
#include "cinder/app/AppNative.h"
#include <vector>

using namespace std;
using namespace ci;

/**
 *  Based on a Fbo PingPong buffer based on openFrameworks
 */
class ciFxBuffer {

public:
    int flag = 0;
    int src = 0;
    int dst = 1;
    
    ciFxBuffer():flag(0){}
    
    gl::FboRef buffers[2];
    
    void allocate(int width,int height,gl::Fbo::Format format){
        
        for (int i = 0; i < 2; ++i) {
            buffers[i] = gl::Fbo::create(width,height,format);
            buffers[i]->bindFramebuffer();
            gl::clear();
            buffers[i]->unbindFramebuffer();
        }
    }
    
    gl::FboRef getSource(){
        return buffers[src];
    }
    
    gl::FboRef getDestination(){
        return buffers[dst];
    }
    
    void bindSource(){
        buffers[src]->bindFramebuffer();
    }
    
    void unbindSource(){
        buffers[src]->unbindFramebuffer();
    }
    
    gl::TextureRef getSourceTexture(){
        return buffers[src]->getColorTexture();
    }
    Area getBounds(){
        return buffers[0]->getBounds();
    }
    void bindDestination(){
        buffers[dst]->bindFramebuffer();
    }
    
    gl::TextureRef getDestinationTexture(){
        return buffers[dst]->getColorTexture();
    }
    
    
    void unbindDestination(){
        buffers[dst]->unbindFramebuffer();
    }
    
    void swap(){
        std::swap(src, dst);
    }
    
};

#endif /* defined(__ciFx__ciFxBuffer__) */
