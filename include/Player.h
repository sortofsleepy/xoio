//
//  Player.h
//  Xoio
//
//  Created by Joseph Chow on 12/20/14.
//
//

#ifndef __Xoio__Player__
#define __Xoio__Player__

#include "Audio.h"
#include <map>
#include <string>

namespace xoio{
    
    class Player : public xoio::Audio {
        
        std::map<std::string,ci::audio::VoiceRef> playlist;
     
    public:
        Player();
        
        /**
         *  Loads a audio file into the playlist. 
         *  Accepts a name in addition to the path to make
         *  things easier to reference.
         */
        void loadFile(std::string name, std::string path);
        
        /**
         *  Plays the specified audio file. If no name is specified,
         *  just starts playing from the top.
         */
        void play(std::string name = "");
        

    };

}
#endif /* defined(__Xoio__Player__) */
