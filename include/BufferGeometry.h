//
//  BufferGeometry.h
//  AttachmentsTest
//
//  Created by Joseph Chow on 1/31/15.
//
//

#ifndef __AttachmentsTest__BufferGeometry__
#define __AttachmentsTest__BufferGeometry__

#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Batch.h"
#include "cinder/Vector.h"
#include "cinder/Camera.h"
#include "ciFxBuffer.h"
#include "cinder/Log.h"
#include <boost/algorithm/string/join.hpp>
#include <string>
#include <vector>
#include <map>


/**
 *  A simple class supporting drawing things to FBOs as well as generating textures
 *  used to pass calculations onto other objects via shaders.
 *
 *  Based off of the work of Patricio Gonzalez Vivo's ofxFx
 *  https://github.com/patriciogonzalezvivo/ofxFX/
 */

class BufferGeometry{
    
protected:
    
    //! the main Glsl program used in calculating values
    ci::gl::GlslProgRef calcshader;
    
    //! buffer used for ping-poning
    ciFxBuffer buffer;
    
    //! textures to use in drawing things to buffers
    std::vector<ci::gl::FboRef> textures;
    
    //! the internal color format to use for the fbo
    int internalFormat;
    
    //! number of passes to use
    int passes;
    
    //! keeps track of whether or not we loaded our shaders
    int shadersLoaded;
    
    //! a counter to be used in "update" to help load default shaders if we've never loaded shaders
    int shaderCount;
    
    //! a internal camera used to help render things onto buffers
    ci::CameraPersp cam;
    
    //! number of textures to render
    int nTextures;
    
    //! keeps track of the currently number of bound textures
    int texturesBound;
    
    //! camera aspect ratio for rendering inside of FBO
    float aspectRatio;
    
    //fov for rendering inside the fbo
    float fov;
    
    //! timer
    float time;
    
    //! reference for drawing things at teh full width/height
    ci::Rectf fullscreen;
    
    //! constant for specifying resolution of the fbo
    ci::vec2 resolution;
    
    //! keep track of mouse position
    ci::vec2 mousePosition;
    
    ci::log::LoggerFile * logger;
    ci::log::LogManager * logManager;
    
    //vector of color attachements to send to each FBO in the duel buffer
    std::vector<GLenum> attachments;

    //! map to be able to cross reference textures by name
    std::map<std::string,ci::gl::FboRef> textureNames;

    //! which index are we currently messing with?
    int currentIndex;
    
    //! temporary reference to texture name
    std::string textureName;
    
    //! internal name to reference when debugging. Based on the shader name.
    std::string internalName;
    
    //! are we debugging?
    bool isDebuging;
    
public:
    BufferGeometry();
    BufferGeometry(int width,int height);
    ~BufferGeometry();
    
    //! initializes all the main components
    virtual void init(int width,int height);
    
    //! returns the number of textures found on the specified shader
    int getNumberOfTextures(std::string path);
    
    //! quickly initializes a fbo
    void initFbo(int width, int height, int internalFormat);
    
    //! loads shaders
    bool loadShaders(std::string name,GLint internalFormat=GL_RGBA);
    
    virtual void update();
    void startUpdate();
    void endUpdate();
    
    virtual void drawTest();
    
    void begin(int texNum=0,std::string name="");
    void end(int textNum=0);
    
    //! generates a random noise texture if we're not drawing something
    void generateTexture();
    
    //!returns the texture at the specified index
    gl::TextureRef getTextureAt(int index);
    
    gl::TextureRef generateTexture(float scale=8.0);
    static gl::TextureRef generateTexture(int width,int height,float scale=8.0);
    
    
    //! Sets a outside uniform. Can either pass in a vec3 or a Texture
    template<typename T>
    void setUniform(std::string name, T& val);
    
    template<typename T>
    void uniform(std::string name, T val);
    
    //! width of the FBO we're writing to
    int width;
    
    //! height of the fbo we're writing to
    int height;
    
    
    gl::TextureRef getResultTexture(){
        return buffer.getDestinationTexture();
    }
};

#endif /* defined(__AttachmentsTest__BufferGeometry__) */
