//
//  Utils.h
//  Nosh
//
//  Created by Joseph Chow on 4/15/15.
//
//

#ifndef Nosh_Utils_h
#define Nosh_Utils_h

#include "cinder/gl/GlslProg.h"

using namespace ci;
using namespace std;
namespace xoio {
    namespace colorutils{
        
        //! changes color objects to a vec4
        static ci::vec4 colorToVec4(const ColorA8u &color){
            return vec4( static_cast<float>( color.r ) / 255.0f,
                        static_cast<float>( color.g ) / 255.0f,
                        static_cast<float>( color.b ) / 255.0f,
                        static_cast<float>( color.a ) / 255.0f );
        }
        
        //! changes color objects to a uint32_t
        static uint32_t colorToIndex(const ci::ColorA8u &color){
            return ((color.a << 24) | (color.r << 16) | (color.g << 8) | (color.b));
        }
        
        //! changes a uint32_t back to a color object
        static ci::ColorA8u indexToColor(uint32_t index){
            return ci::ColorA8u( ( ( index >> 16 ) & 0xFF ), ( ( index >> 8 ) & 0xFF ), ( index & 0xFF ), ( ( index >> 24 ) & 0xFF ) );
        }
        
        
    }
    
    namespace shaderutils {
        
        //! wrapper to make a GlslProgRef object
        ci::gl::GlslProgRef makeShader(string vertex,string fragment){
            ci::gl::GlslProg::Format format;
            format.vertex(app::loadAsset(vertex));
            format.fragment(app::loadAsset(fragment));
            return gl::GlslProg::create(format);
        }
        
        
        //! creates a shader for use when doing TransformFeedback
        ci::gl::GlslProgRef makeFeedbackShader(string vertex,string fragment,map<string,int> attributes,  std::vector<std::string> varyings,GLuint feedbackFormat=GL_INTERLEAVED_ATTRIBS){
            
            
            gl::GlslProg::Format format;
            format.vertex(app::loadAsset(vertex));
            format.fragment(app::loadAsset(fragment));
            
            format.feedbackFormat(feedbackFormat);
            
            //apply attributes to shader
            map<string,int>::iterator it = attributes.begin();
            for(;it != attributes.end();++it){
                format.attribLocation(it->first, it->second);
            }
            
            //apply varyings
            if(varyings.size() < 1){
                app::console()<<"0 varyings were found. Did you forget to add some?\n";
            }else{
                //apply varyings to shader
                format.feedbackVaryings(varyings);
            }
            
            //return shader
            return gl::GlslProg::create(format);
        }
        
    }
    
    namespace mathutils {
        //! port of Processing's "constrain" function.
        //! see https://github.com/processing/processing/blob/master/core/src/processing/core/PApplet.java#L3927
        static float constrain(float amt,float low,float high){
            return (amt < low) ? low  : ((amt > high) ? high : amt);
        }
    }
}

#endif
