//
//  Path.h
//  ParticlePaths
//  Creates a path that can optionally draw itself for debugging purposes.
//  Created by Joseph Chow on 8/4/15.
//
//

#ifndef __ParticlePaths__Path__
#define __ParticlePaths__Path__

#include "cinder/BSpline.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/Shader.h"
#include <vector>

namespace xoio {
    class Path {
        //!batch to draw all things
        ci::gl::BatchRef batch;
        
        //! mesh to store sine wave data
        ci::gl::VboMeshRef mVboMesh;
        
        //render shader
        ci::gl::GlslProgRef render;
        
        //vector of all the points in the path
        std::vector<ci::vec3> points;
        
        //! determines if we should draw a line to show the path.
        bool shouldDrawLine = false;
        
        //! spline for path interpolation
        ci::BSpline3f spline;
        
        //! properties for the spline
        bool loop = false;
        bool open = true;
        int degree = 3;
    public:
        Path();
        Path(int degree,bool loop, bool open);
        
        ci::BSpline3f getSpline();
        ci::gl::VboMeshRef getMeshPath();
        
        //! adds one point to the path
        void addPoint(ci::vec3 point);
        
        //! adds a set of points to the path
        void addPoints(std::vector<ci::vec3> points);
        
        //! toggles whether or not to draw the line
        void toggleDebugline();
        
        //!initis the spline and vbo for drawing
        void constructPath();
        
        void draw();
    };
}
#endif /* defined(__ParticlePaths__Path__) */
