//
//  Audio.h
//  Xoio
//
//  Created by Joseph Chow on 12/20/14.
//
//

#ifndef __Xoio__Audio__
#define __Xoio__Audio__

#include "cinder/audio/Context.h"
#include "cinder/audio/Buffer.h"
#include "cinder/audio/Voice.h"
#include "cinder/app/AppNative.h"
#include "cinder/audio/MonitorNode.h"
namespace xoio {
    class Audio {
        
    protected:
        //node used for getting back information about the audio.
        ci::audio::NodeRef monitor;
        
        //to make sure that a monitor node was created before attempting anything
        bool monitorCreated;
        
    public:
        Audio();
        
        void setupMonitor();
        
        /**
         *  Loads a audio file for playback.
         */
        ci::audio::VoiceRef loadAudio(ci::DataSourceRef file);
        
        /**
         *  Get a ref to the Monitor node
         */
        ci::audio::NodeRef getMonitor();
        
        /**
         *  Some wrapper functions around Monitor node
         *  functionality.
         */
        size_t getChannels();
        size_t getSampleRate();
        ci::audio::Buffer* getBuffer();
    };
}; //end namespace

#endif /* defined(__Xoio__Audio__) */
