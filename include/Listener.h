//
//  Listener.h
//  Xoio
//
//  Created by Joseph Chow on 1/6/15.
//
//

#ifndef Xoio_Listener_h
#define Xoio_Listener_h


#include <stdio.h>
#include <boost/signals2.hpp>
#include <vector>
#include <string>

/**
 *  A very generalized object listener class.
 */
template<typename T>
class Listener{
    
    //signals object to handle triggering callbacks
    boost::signals2::signal<void()> signal;
    
    //the value we're watching for a change
    T * watchedValue;
    
    //have we set a watched value yet?
    bool initialized;
    
public:
    
    Listener(){
        initialized = false;
    }
    ~Listener(){}
    
    /**
     *  Sets up the value we want to observe.
     */
    void watch(T * value){
        set(value);
    }
    
    void set(T * value){
        watchedValue = value;
        
        //if we've just initialized a value, don't do anything
        if(initialized == false){
            initialized = true;
            
            //otherwise the value got changed, trigger our callbacks
        }else{
            //trigger the callback when the value changes.
            trigger();
        }
    }
    
    /**
     *  Adds a callback to the signal stack.
     */
    template<typename A>
    void addCallback(void (A::* callbackFunction)(), A * callbackObject){
        signal.connect(boost::function<void ()>(boost::bind(callbackFunction, callbackObject)));
    }
    
    /**
     *  Triggers all the callbacks to run.
     */
    void trigger(){
        signal();
    }
    
};


#endif
