//
//  FboBuffer.h
//  Xoio
//
//  Created by Joseph Chow on 11/8/14.
//
//

#ifndef __Xoio__FboBuffer__
#define __Xoio__FboBuffer__

#include "cinder/gl/Fbo.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/Surface.h"
#include "cinder/Rand.h"
#include <vector>
using namespace ci;
using namespace std;

// Color attachments to hold scene data
static const GLenum kColorAttachments[ 3 ] = {
    GL_COLOR_ATTACHMENT0,	// Position
    GL_COLOR_ATTACHMENT1,	// Velocity
    GL_COLOR_ATTACHMENT2	// Acceleration
};
class FboBuffer{
    
    ci::gl::FboRef fbos[2];
    
    ci::vec2 size;
    
    ci::gl::GlslProgRef renderShader;
    
    int flag;
    
    vector<ci::gl::GlslProgRef> passes;
    std::vector<GLenum> mAttachments;
    
    gl::GlslProgRef calc;
    gl::GlslProgRef render;
    
    Surface32f initData;
    gl::TextureRef initTexture;
    
public:
    FboBuffer();
    FboBuffer(int width,int height);
    
    void setup();
    void swap();
    void update();
    
    void draw();
    
    
    
};


#endif /* defined(__Xoio__FboBuffer__) */
