//
//  Path.cpp
//  ParticlePaths
//
//  Created by Joseph Chow on 8/4/15.
//
//

#include "Path.h"

using namespace ci;
using namespace std;

namespace xoio {
    Path::Path(){}
    Path::Path(int degree,bool loop, bool open){
        this->degree = degree;
        this->loop = loop;
        this->open = open;
    }
  
    void Path::addPoint(ci::vec3 point){
        points.push_back(point);
    }
    
    void Path::addPoints(std::vector<ci::vec3> points){
        for(int i = 0; i < points.size(); ++i ){
            this->points.push_back(points.at(i));
        }
    }
    
    ci::BSpline3f Path::getSpline(){
        return spline;
    }
    
    ci::gl::VboMeshRef Path::getMeshPath(){
        return mVboMesh;
    }
    
    void Path::constructPath(){
        spline = ci::BSpline3f(points,degree,loop,open);
        
        render = gl::getStockShader(gl::ShaderDef().color());
        vec4 color = vec4(255,255,0,1);
    
        std::vector<vec4> vertexColors( points.size(),color );
        
        gl::VboMesh::Layout layout;
        layout.usage( GL_DYNAMIC_DRAW ).attrib( geom::POSITION, 3 ).attrib( geom::COLOR, 4 );
        
        mVboMesh = gl::VboMesh::create( points.size(), GL_LINE_STRIP, { layout } );
        mVboMesh->bufferAttrib( geom::POSITION, points.size() * sizeof( vec3 ), points.data() );
        mVboMesh->bufferAttrib( geom::COLOR, vertexColors );
        
        batch = gl::Batch::create(mVboMesh, render);

    }
    
    
    
    void Path::toggleDebugline(){
        shouldDrawLine = !shouldDrawLine;
    }
    
    void Path::draw(){
        if(shouldDrawLine){
            batch->draw();
        }
    }
};