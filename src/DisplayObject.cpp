//
//  DisplayObject.cpp
//  Xoio
//
//  Created by Joseph Chow on 12/20/14.
//
//

#include "DisplayObject.h"
using namespace ci;
using namespace std;
namespace xoio {
    
    DisplayObject::DisplayObject(){}
    
    
    //loads default rendering shader
    void DisplayObject::loadRenderShader(string path){
        if(path != ""){
            gl::GlslProg::Format format;
            format.vertex(app::loadAsset(path + "_vert.glsl"));
            
            format.fragment(app::loadAsset(path + "_frag.glsl"));
            
            renderShader = gl::GlslProg::create(format);
        }else{
            renderShader = gl::getStockShader(gl::ShaderDef().color());
        }
    }
    
    void DisplayObject::vertex(ci::vec3 position){
        vertices.push_back(position);
    }
    
    void DisplayObject::addVarying(std::string name){
        varyings.push_back(name);
    }
    
    /**
     *  Adds a attribute to the stack
     */
    void DisplayObject::addAttribute(string name, int size){
        Attribute vao;
        vao.name = name;
        vao.size = size;
        vao.index = attributeCounter;
        attributeData.push_back(vao);
        attributeCounter++;
    }
    
    /**
     *  Enables a attribute
     *  @param{string} name the name of the attribute you want to enable
     */
    void DisplayObject::enableAttribute(string name){
        
        if(attributes == NULL){
            attributes = gl::Vao::create();
        }
        vector<Attribute>::iterator it = attributeData.begin();
        gl::ScopedVao vao(attributes);
        for(;it != attributeData.end();++it){
            if(it->name == name){
                gl::enableVertexAttribArray(it->index);
            }
        }
    }
}