//
//  BufferGeometry.cpp
//  AttachmentsTest
//
//  Created by Joseph Chow on 1/31/15.
//
//

#include "BufferGeometry.h"
#include "cinder/gl/Shader.h"
BufferGeometry::BufferGeometry(){}
BufferGeometry::BufferGeometry(int width,int height){
    init(width,height);
}

BufferGeometry::~BufferGeometry(){}


void BufferGeometry::init(int width, int height){
    this->width = width;
    this->height = height;
    passes = 5;
    time = 0;
    gl::Fbo::Format format;
    buffer.allocate(width, height, format);
    shadersLoaded = false;
    shaderCount = 0;
    texturesBound = 0;
    fullscreen = Rectf(0,0,width,height);
    resolution = vec2(width,height);
    
    isDebuging = true;

    //bind to mousemove to update mouse position
    app::getWindow()->getSignalMouseMove().connect([this](cinder::app::MouseEvent e){
        mousePosition = e.getPos();
    });

}

int BufferGeometry::getNumberOfTextures(std::string path){
    string line;
    vector<string> lines;
    ifstream myfile;
    int num = 0;
    
    if(fs::exists(app::getAssetPath(path))){
        myfile.open(app::getAssetPath(path).string().c_str(),ifstream::in);
        if(myfile.is_open()){
            while(myfile.good()){
                getline(myfile,line);
                lines.push_back(line);
            }
        }
    }
    gl::Fbo::Format fmat;
    string joined = boost::algorithm::join(lines, " ");
    //! look for the keyword "tex" + index to figure out how many textures we need to worry about.
    for(int i = 0; i < 10;++i){
        string searchFor = "tex" + std::to_string(i);
        if(joined.find(searchFor) != -1){
            num++;
        }
    }

    
    //if we have no textures declared, then add at least allocate one fbo so we
    //have textures to set data to.
    if(num == 0){
        num++;
    }
    
    if(isDebuging){
        app::console()<<"Number of textures in " << internalName << " : "<<num<<"\n";
    }
    
    return num;
    
}

void BufferGeometry::initFbo(int width, int height, int internalFormat){

        gl::FboRef _fbo;
        gl::Fbo::Format fmt;
        gl::Texture::Format tformat;
        
        //setup formats of textures and Fbo
        tformat.setInternalFormat(internalFormat);
        fmt.setColorTextureFormat(tformat);
        
        //assign some attachments
        for(int i = 0;i<attachments.size();++i){
            fmt.attachment(attachments.at(i), gl::Texture2d::create(width, height,tformat));
        }
        
        _fbo = gl::Fbo::create(width, height,fmt);
        
        _fbo->bindFramebuffer();
        gl::clear();
        _fbo->unbindFramebuffer();
        
        
        textures.push_back(_fbo);
    
}

void BufferGeometry::update(){
    time = app::getElapsedSeconds();
    
    //set the viewport and window transforms to match the size of the Fbo
    gl::setMatricesWindow( vec2(width,height));
    gl::viewport( vec2(width,height) );
    
    //loop through passes
    for(int pass = 0; pass < passes; ++pass){
        gl::ScopedFramebuffer fBuff(buffer.getDestination());{
            //disable alpha blending for now
            gl::disableAlphaBlending();
            
            //bind calcuclation shader
            gl::ScopedGlslProg shaderBind(calcshader);{
                
                //loop through the textures we need to send
                for(int i = 0;i<textures.size();++i){
                    getTextureAt(i)->bind(i);
                    calcshader->uniform("tex" + std::to_string(i), i);
                }
                
                //make sure to send previous buffer as well
                buffer.getSourceTexture()->bind(0);
                calcshader->uniform("backbuffer",0);
                
                //! draw something to hold the data
                gl::drawSolidRect(fullscreen);
                
                //send some useful vars to the shader
                //calcshader->uniform("resolution", resolution);
                //calcshader->uniform("time", time);
                //calcshader->uniform("mousePosition", mousePosition);
            }
            
        } //end destination buffer scope
        
        buffer.swap();
    }
    buffer.swap();
}

void BufferGeometry::startUpdate(){
    time = app::getElapsedSeconds();
    
    //set the viewport and window transforms to match the size of the Fbo
    gl::setMatricesWindow( vec2(width,height));
    gl::viewport( vec2(width,height) );
    //loop through passes
    for(int pass = 0; pass < passes; ++pass){
        buffer.getDestination()->bindFramebuffer();
        gl::disableAlphaBlending();
        calcshader->bind();
        
        //loop through the textures we need to send
        for(int i = 0;i<textures.size();++i){
            getTextureAt(i)->bind(i);
            calcshader->uniform("tex" + std::to_string(i), i);
            
        }
        
    }
    
}

void BufferGeometry::endUpdate(){
    for(int pass = 0; pass < passes; ++pass){
        //! draw something to hold the data
        gl::drawSolidRect(fullscreen);
        buffer.getDestination()->unbindFramebuffer();
        buffer.swap();
    }
    buffer.swap();
}

template<typename T>
void BufferGeometry::uniform(std::string name, T val){
    calcshader->uniform(name, val);
}

void BufferGeometry::drawTest(){
    gl::enableAlphaBlending();
    gl::setMatricesWindow( app::getWindowSize() );
    gl::viewport( app::getWindowSize() );
   // gl::draw(textures.at(0)->getColorTexture(),Rectf(0,0,width,height));
    
    //gl::draw(buffer.getDestinationTexture(),Rectf(0,0,width,height));
    
    buffer.getDestinationTexture()->bind();{
        gl::ScopedGlslProg shaderScp( gl::getStockShader( gl::ShaderDef().texture() ) );
        gl::drawSolidRect(Rectf(0,0,200,200));
    }
}

bool BufferGeometry::loadShaders(std::string name,GLint internalFormat){
    string vertName = name + ".vert.glsl";
    string fragName = name + ".frag.glsl";
    gl::GlslProg::Format format;
    int num = 0;
    
    internalName = name;
    
   
    
    //CREATE SHADER PROGRAM
    /**
     *  If the vertex is empty but we have a fragment
     */
    if(!fs::exists(app::getAssetPath(vertName))){
        if(fs::exists(app::getAssetPath(fragName))){
            format.vertex(app::loadAsset("default.vert.glsl"));
            format.fragment(app::loadAsset(fragName));
            
            //get the number of textures from fragment
            num = getNumberOfTextures(fragName);
        }
        
        return false;
    }
    
    /**
     *  If the fragment is empty but we have a vertex
     */
    if(!fs::exists(app::getAssetPath(fragName))){
        if(fs::exists(app::getAssetPath(vertName))){
            format.vertex(app::loadAsset(vertName));
            format.fragment(app::loadAsset("default.frag.glsl"));
            num = getNumberOfTextures(vertName);
        }
        
        return false;
    }
    
    //if we have both
    format.vertex(app::loadAsset(vertName));
    format.fragment(app::loadAsset(fragName));
    num = getNumberOfTextures(fragName);
    
    
    nTextures = num;
    calcshader = gl::GlslProg::create(format);
    cam = CameraPersp(width,height,60.0f);
    cam.setPerspective(60, app::getWindowAspectRatio(), 1, 1000);
    
    
    //initiate the appropriate number of FBOs
    for(int i = 0;i<num;++i){
        initFbo(width, height, internalFormat);
    }
    
    shadersLoaded = true;
   
    return true;


}


void BufferGeometry::begin(int texNum,string name){
       if(name != ""){
        //set current index so we cn save
        currentIndex = texNum;
        textureName = name;
    }
    if ((texNum < nTextures) && ( texNum >= 0)){
        gl::pushMatrices();
        gl::setMatricesWindow( textures[texNum]->getSize()  );
        gl::viewport( textures[texNum]->getSize() );
        textures[texNum]->bindFramebuffer();
    }
}

void BufferGeometry::end(int texNum){
  
    if ((texNum < nTextures) && ( texNum >= 0)){
        textures[texNum]->unbindFramebuffer();
        gl::popMatrices();
    }
    
    //store the texture so we can do a quick lookup later by name
    if(currentIndex != 0){
        textureNames.insert(pair<string,gl::FboRef>(textureName,textures[texNum]));
        currentIndex = 0;
        textureName = "";
    }
}

gl::TextureRef BufferGeometry::getTextureAt(int index){
    return textures[index]->getColorTexture();
}


gl::TextureRef BufferGeometry::generateTexture(float scale){
    Surface32f surface = Surface32f(width,height,true);
    
    Surface32f::Iter it = surface.getIter();
    
    while (it.line()) {
        while(it.pixel()){
            surface.setPixel(it.getPos(),
                             ColorAf(scale*(Rand::randFloat()-0.5f),
                                     scale*(Rand::randFloat()-0.5f),
                                     scale*(Rand::randFloat()-0.5f),
                                     Rand::randFloat(0.2f, 1.0f) ) );
        }
    }
    
    return gl::Texture::create(surface);
}

gl::TextureRef BufferGeometry::generateTexture(int width,int height,float scale){
    Surface32f surface = Surface32f(width,height,true);
    
    Surface32f::Iter it = surface.getIter();
    
    gl::Texture::Format format;
    format.internalFormat(GL_RGBA);
    format.setMagFilter(GL_NEAREST);
    format.setMinFilter(GL_NEAREST);
    format.setWrap(GL_REPEAT, GL_REPEAT);
    
    while (it.line()) {
        while(it.pixel()){
            surface.setPixel(it.getPos(),
                             ColorAf(scale*(Rand::randFloat()-0.5f),
                                     scale*(Rand::randFloat()-0.5f),
                                     scale*(Rand::randFloat()-0.5f),
                                     Rand::randFloat(0.2f, 1.0f) ) );
        }
    }
    
    return gl::Texture::create(surface,format);
}
