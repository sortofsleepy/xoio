//
//  FboBuffer.cpp
//  Xoio
//
//  Created by Joseph Chow on 11/8/14.
//
//

#include "FboBuffer.h"
FboBuffer::FboBuffer(){}
FboBuffer::FboBuffer(int width,int height){
    size = vec2(width,height);
}

void FboBuffer::setup(){  gl::Fbo::Format format;
    gl::Texture::Format tformat;
    
    tformat.setInternalFormat(GL_RGBA32F);
    
    format.setColorTextureFormat(tformat);
    
    
    // Set up OpenGL for data
    gl::disableDepthRead();
    gl::disableDepthWrite();
    size = vec2(app::getWindowWidth(),app::getWindowHeight());
    
    
    
    // Set everything to 0
    flag = 0;
    swap();
    flag = 0;
    gl::GlslProg::Format sformat;
    sformat.vertex(app::loadAsset("pass.vert"));
    sformat.fragment(app::loadAsset("pass_calc.frag"));
    
    calc = gl::GlslProg::create(sformat);
    
    gl::GlslProg::Format ssformat;
    ssformat.vertex(app::loadAsset("shader.vert"));
    ssformat.fragment(app::loadAsset("shader.frag"));
    
    render = gl::GlslProg::create(ssformat);
    gl::Fbo::getMaxAttachments();
    float scale = 500.0f;
    ColorAf test2 = ColorAf(scale*(Rand::randFloat()-0.5f),
                            scale*(Rand::randFloat()-0.5f),
                            scale*(Rand::randFloat()-0.5f),
                            Rand::randFloat(0.2f, 1.0f) ) ;
    
    
    app::console()<<test2;
    
    initData = Surface32f(app::getWindowWidth(),app::getWindowHeight(),true);
    
    Surface32f::Iter pixelIter = initData.getIter();
    while( pixelIter.line() ) {
        while( pixelIter.pixel() ) {
            /* Initial particle positions are passed in as R,G,B
             float values. Alpha is used as particle invMass. */
            initData.setPixel(pixelIter.getPos(),
                              ColorAf(scale*(Rand::randFloat()-0.5f),
                                      scale*(Rand::randFloat()-0.5f),
                                      scale*(Rand::randFloat()-0.5f),
                                      Rand::randFloat(0.2f, 1.0f) ) );
        }
    }
    
    initTexture = gl::Texture::create(initData);
    
    for(int i = 0;i<2;++i){
        fbos[i] = gl::Fbo::create(size.x,size.y,format);
        
        fbos[i]->bindFramebuffer();
        gl::clear();
        gl::draw(initTexture);
        fbos[i]->unbindFramebuffer();
        
    }
}

void FboBuffer::swap(){
    flag = !flag;
}

void FboBuffer::update(){
    //bind the framebuffer. Not unbinding at the end for some reason so we use the normal way
    fbos[flag]->bindFramebuffer();
    
    //bind the first shader of the pass
    gl::ScopedGlslProg c(calc);
    
    //send the previous texture to the shader
    gl::ScopedTextureBind tex(fbos[!flag]->getColorTexture(),0);
    calc->uniform("buffer",0);
    
    //draw a rect to collect the new calculations
    gl::drawSolidRect( fbos[flag]->getBounds() );
    
    //unbind
    fbos[flag]->unbindFramebuffer();
    
    //swap
    swap();
    
}

void FboBuffer::draw(){
    
    //bind the second shader of the pass
    gl::ScopedGlslProg re(render);
    
    //send the texture from the previous fbo to the shader
    gl::ScopedTextureBind tex(fbos[!flag]->getColorTexture(),0);
    render->uniform("buffer",0);
    
}