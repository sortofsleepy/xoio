#include "cinder/app/AppNative.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "Player.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class XoioApp : public AppNative {
  public:
	void setup() override;
    void prepareSettings(Settings * settings);
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;

    xoio::Player audioPlayer;
};


void XoioApp::prepareSettings(cinder::app::AppBasic::Settings *settings){
    settings->setWindowSize(1024, 768);
}

void XoioApp::setup()
{
   
}

void XoioApp::mouseDown( MouseEvent event )
{

  
}

void XoioApp::update()
{
 

}

void XoioApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );

}

CINDER_APP_NATIVE( XoioApp, RendererGl )
