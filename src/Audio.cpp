//
//  Audio.cpp
//  Xoio
//
//  Created by Joseph Chow on 12/20/14.
//
//

#include "Audio.h"

using namespace ci;
using namespace std;

namespace xoio {
    Audio::Audio():monitorCreated(false){}
    
    void Audio::setupMonitor(){
        auto ctx = audio::Context::master();
        monitor = ctx->makeNode(new audio::MonitorNode);
    }
    audio::VoiceRef Audio::loadAudio(ci::DataSourceRef file){
        auto sourceFile = audio::load(file);
        audio::VoiceRef source = audio::Voice::create(sourceFile);
        return source;
    }
    
    ci::audio::NodeRef Audio::getMonitor(){
        if(monitorCreated){
            return monitor;
        }else{
            setupMonitor();
            monitorCreated = true;
            return monitor;
        }
    }
    
    /**
     *  Wrapper function around the MonitorNode object to
     *  get the number of channels.
     */
    size_t Audio::getChannels(){
        return monitor->getNumChannels();
    };
    
    /**
     *  Get the sample rate of the current output
     */
    size_t Audio::getSampleRate(){
        return monitor->getSampleRate();
    };
    
    
    /**
     *  Gests the buffer of the current output
     */
    ci::audio::Buffer* Audio::getBuffer(){
        return monitor->getInternalBuffer();
    }
    

}; //end namespace