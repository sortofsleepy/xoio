//
//  QueryParams.cpp
//  Nosh
//
//  Created by Joseph Chow on 5/13/15.
//
//

#include "QueryParams.h"

using namespace std;
namespace xoio{
    namespace network{
        QueryParams::QueryParams(){}
        void QueryParams::addParam(std::string name, std::string param){
            params.insert(pair<string,string>(name,param));
        }
        
        string QueryParams::getValue(std::string key){
            return params[key];
        }
        
        string QueryParams::getString(){
        
            std::map<string,string>::iterator it = params.begin();
            for(;it != params.end();++it){
                int index = std::distance(std::begin(params), params.find(it->first));
                if(index == 0){
                    query += it->first + "=" + it->second;
                }else{
                    query += "&" + it->first + "=" + it->second;
                 }
            }
            
            return query;
        }
    }
}