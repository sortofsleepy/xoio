//
//  Request.cpp
//  Nosh
//
//  Created by Joseph Chow on 5/12/15.
//
//

#include "Request.h"
#include "cinder/app/App.h"
using namespace ci;
using namespace std;

namespace xoio {
    namespace network {
        Request::Request(){}
        
        void Request::buildEndpoint(boost::asio::io_service service,std::string urlstring,int port){
           
            
            //replace http portion
            string parsed = boost::replace_all_copy(urlstring,"http://","");
  
            if(port != 80){
                boost::asio::ip::tcp::resolver::query query( parsed, std::to_string(port) );
                boost::asio::ip::tcp::resolver resolver( service );
                
                try {
                    boost::asio::ip::tcp::resolver::iterator destination = resolver.resolve(query);
                    
                    boost::asio::ip::tcp::endpoint endpoint;
                    while ( destination != boost::asio::ip::tcp::resolver::iterator() )
                        endpoint = *destination++;
                    
                    mEndPoint = endpoint;
                }
                catch(const std::exception &e) {
                    ci::app::console() << "Server exception:" << e.what() << std::endl;
                }

            }else{
                boost::asio::ip::tcp::resolver::query query( parsed, "daytime" );
                boost::asio::ip::tcp::resolver resolver( service );
                
                try {
                    boost::asio::ip::tcp::resolver::iterator destination = resolver.resolve(query);
                    
                    boost::asio::ip::tcp::endpoint endpoint;
                    while ( destination != boost::asio::ip::tcp::resolver::iterator() )
                        endpoint = *destination++;
                    
                    
                    mEndPoint = endpoint;
                    
                }
                catch(const std::exception &e) {
                    ci::app::console() << "Server exception:" << e.what() << std::endl;
                }

            }
           
        }
    }
}
