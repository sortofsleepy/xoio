//
//  HttpClient.cpp
//  Nosh
//
//  Created by Joseph Chow on 5/12/15.
//
//

#include "HttpClient.h"

using namespace std;
using namespace ci;

namespace xoio {
    namespace network {
        HttpClient::HttpClient():mSocket(ioservice),mReconnectTimer(ioservice){
        }
        
        void HttpClient::disconnect(){
            
            //close the socket
            mSocket.close();
            
        }
        
        void HttpClient::read(){
            if(!isConnected) return;
            if(isClosing) return;
            
            // wait for a message to arrive, then call handle_read
            boost::asio::async_read_until(mSocket, mBuffer, mDelimiter,
                                          boost::bind(&HttpClient::handle_read, this, boost::asio::placeholders::error));
        }
        void HttpClient::close(){}
        
        void HttpClient::do_close(){
            if(isClosing){
                return;
            }
            
            isClosing = true;
            mSocket.close();
        }
        
        void HttpClient::do_reconnect(const boost::system::error_code &error){
            
        }
        
        void HttpClient::do_heartbeat(const boost::system::error_code &error){
            
        }
        
        void HttpClient::connect(boost::asio::ip::tcp::endpoint &endpoint){
            if(isConnected) return;
            if(isClosing) return;
            
#ifdef CINDER_MAC
            ci::app::console() << "Trying to connect to port " << endpoint.port() << " @ " << endpoint.address().to_string() << std::endl;
#endif
            // try to connect, then call handle_connect
            mSocket.async_connect(endpoint,
                                  boost::bind(&HttpClient::handle_connect, this, boost::asio::placeholders::error));
        }
        
        
        
        
        ///////// INTERNAL CALLBACKS //////////
        //! connect callback
        void HttpClient::handle_connect(const boost::system::error_code &error){
            if(isClosing) return;
            
            if (!error) {
                // we are connected!
                isConnected = true;
                
                // let listeners know
                sConnected(mEndPoint);
                
                // start heartbeat timer (optional)
                // mHeartBeatTimer.expires_from_now(std::chrono::seconds(5));
                //mHeartBeatTimer.async_wait(boost::bind(&HttpClient::do_heartbeat, this, boost::asio::placeholders::error));
                
                // await the first message
                read();
            }
            else {
                // there was an error :(
                isConnected = false;
                
                //
                ci::app::console() << "Server error:" << error.message() << std::endl;
                
                // schedule a timer to reconnect after 5 seconds
                mReconnectTimer.expires_from_now(std::chrono::seconds(5));
                mReconnectTimer.async_wait(boost::bind(&HttpClient::do_reconnect, this, boost::asio::placeholders::error));
            }
            
        }
        //! read callback
        void HttpClient::handle_read(const boost::system::error_code& error)
        {
            if (!error)
            {
                std::string msg;
                std::istream is(&mBuffer);
                std::getline(is, msg);
                
                if(msg.empty()) return;
                
                ci::app::console() << "Server message:" << msg << std::endl;
                
                // TODO: you could do some message processing here, like breaking it up
                //       into smaller parts, rejecting unknown messages or handling the message protocol
                
                // create signal to notify listeners
                sMessage(msg);
                
                // restart heartbeat timer (optional)
                //mHeartBeatTimer.expires_from_now(std::chrono::seconds(5));
                //  mHeartBeatTimer.async_wait(boost::bind(&HttpClient::do_heartbeat, this, boost::asio::placeholders::error));
                
                // wait for the next message
                read();
            }
            else
            {
                // try to reconnect if external host disconnects
                if(error.value() != 0) {
                    isConnected = false;
                    
                    // let listeners know
                    sDisconnected(mEndPoint);
                    
                    // cancel timers
                    // mHeartBeatTimer.cancel();
                    mReconnectTimer.cancel();
                    
                    // schedule a timer to reconnect after 5 seconds
                    mReconnectTimer.expires_from_now(std::chrono::seconds(5));
                    mReconnectTimer.async_wait(boost::bind(&HttpClient::do_reconnect, this, boost::asio::placeholders::error));
                }
                else
                    do_close();
            }
            
        }
        
        
        //! write callback
        void HttpClient::handle_write(const boost::system::error_code& error)
        {
        }
        
        void HttpClient::do_write(const std::string &msg)
        {
        }
        
        
        
        
        
    }
}