//
//  Player.cpp
//  Xoio
//
//  Created by Joseph Chow on 12/20/14.
//
//

#include "Player.h"

using namespace ci;
using namespace std;


namespace xoio {
    Player::Player(){
      
    }
    
    void Player::loadFile(string name, string path){
        //setup the monitor so we can collect data
        getMonitor();
        
        audio::VoiceRef source = loadAudio(app::loadResource(path));
        playlist.insert(pair<string,audio::VoiceRef>(name,source));
    }
    
    
    void Player::play(string name){
        if(name == ""){
            
            map<string,audio::VoiceRef>::iterator it = playlist.end();
            it--;
            it->second->start();
        }else{
            map<string,audio::VoiceRef>::iterator it = playlist.begin();
            for(;it != playlist.end();++it){
                if(it->first == name){
                    it->second->start();
                }
            }
        }
    }
    
      
    
    
}