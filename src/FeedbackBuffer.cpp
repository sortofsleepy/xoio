//
//  FeedbackBuffer.cpp
//  Lettering
//
//  Created by Joseph Chow on 3/2/15.
//
//

#include "FeedbackBuffer.h"

using namespace ci;
using namespace std;

namespace xoio{
    FeedbackBuffer::FeedbackBuffer():feedbackFormat(GL_INTERLEAVED_ATTRIBS),
    primativeType(GL_POINTS){}
    void FeedbackBuffer::setupBuffer(GLenum target,GLsizeiptr allocationSize,const void *data,GLenum usage){
        
        mBuffer[0] = gl::Vbo::create(target,allocationSize,data,usage);
        
        mBuffer[1] = gl::Vbo::create(target,allocationSize,data,usage);
        
        mAttributes[0] = gl::Vao::create();
        mAttributes[1] = gl::Vao::create();
    }
    
    void FeedbackBuffer::enableAttribute(string name,int index){
        enabledAttributes.insert(pair<string,int>(name,index));
    }
    
    void FeedbackBuffer::vertexAttribPointer(GLuint index,GLint size,GLenum type,GLboolean normalized,GLsizei stride,const GLvoid * pointer){
        
        for(int i = 0;i<2;++i){
            gl::ScopedVao vao( mAttributes[i] );
            gl::ScopedBuffer buffer( mBuffer[i] );
            
            std::map<string,int>::iterator it = enabledAttributes.begin();
            
            for(;it != enabledAttributes.end();++it){
                gl::enableVertexAttribArray(it->second);
            }
            
            //pointer with that data
            gl::vertexAttribPointer(index, size, type, normalized, stride, pointer);
        }
    }
    
    
    void FeedbackBuffer::updateBuffers(){
        
    }
    
    void FeedbackBuffer::addVarying(std::string name){
        varyings.push_back(name);
    }

    
    void FeedbackBuffer::setShaders(string vertex,string fragment,bool forRender){
        
        gl::GlslProg::Format format;
        format.vertex(app::loadAsset(vertex));
        format.fragment(app::loadAsset(fragment));
        
        
        if(!forRender){
            format.feedbackFormat(feedbackFormat);
            //apply attributes to shader
            map<string,int>::iterator it = enabledAttributes.begin();
            for(;it != enabledAttributes.end();++it){
                format.attribLocation(it->first, it->second);
            }
            
            if(varyings.size() < 1){
                app::console()<<"0 varyings were found. Did you forget to add some?\n";
            }else{
                //apply varyings to shader
                format.feedbackVaryings(varyings);
            }
            shader = gl::GlslProg::create(format);
            
            //use default shader for rendering
            renderShader = gl::getStockShader(gl::ShaderDef().color());
        }else{
            renderShader = gl::GlslProg::create(format);
        }
    }

    
    
}